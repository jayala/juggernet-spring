# README #

This project is my first attempt at creating the structure for a Java Spring web application. I'm trying to learn the prevailing methods and tools used by Java developers. I started the project as a single Maven project and have broken it out into several components. I've set up Hibernate and JPA. I've set up the MySQL connection, as well as HSQL for testing. Tomcat can be run embedded, and JNDI is used for the MySQL connection. As I work through the several tutorials and examples I'm sure I'll find other tools and ideas to use.

### How do I get set up? ###

* checkout the source code and do a maven build on the root POM
    * $> maven install
    * $> maven tomcat6:run