package com.juggernet.webapp.controller;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/home")
public class HomeController {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@RequestMapping(method = RequestMethod.GET)
	public void index(ModelMap map) {

		logger.info("index()");

		map.addAttribute("message", "hello world!");

		try {
			// Obtain our environment naming context
			Context initCtx = new InitialContext();
			logger.trace("got initial context");
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			logger.trace("got env context");
			// Look up our data source
			DataSource ds = (DataSource) envCtx.lookup("jdbc/JuggernetLocalDb");
			logger.trace("got datasource");
			// Allocate and use a connection from the pool
			Connection conn = ds.getConnection();
			logger.trace("got connection");
			conn.close();
			logger.info("closed connection");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}