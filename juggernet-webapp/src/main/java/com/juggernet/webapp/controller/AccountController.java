package com.juggernet.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.juggernet.interfaces.service.IAccountService;

@RestController
@RequestMapping("/account")
public class AccountController {

	private IAccountService _accountService;

	@Autowired
	public AccountController(IAccountService accountService) {
		_accountService = accountService;
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public String sayHello(@PathVariable("name") String name) {

		Boolean authenticated = _accountService.Authenticate(name, "");
		if (authenticated)
			return "yes";
		return "no";
	}
}
