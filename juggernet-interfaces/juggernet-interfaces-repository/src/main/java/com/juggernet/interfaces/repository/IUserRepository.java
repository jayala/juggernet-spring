package com.juggernet.interfaces.repository;

import com.juggernet.interfaces.domain.IUser;

public interface IUserRepository {

	public abstract IUser getUser(final Long id);

}
