package com.juggernet.interfaces.domain;


public interface IUser {

	public abstract void setLastName(String lastName);

	public abstract String getLastName();

	public abstract void setFirstName(String firstName);

	public abstract String getFirstName();

	public abstract void setId(Long id);

	public abstract Long getId();

}
