package com.juggernet.impl.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juggernet.impl.domain.User;
import com.juggernet.impl.repository.UserRepository;
import com.juggernet.interfaces.service.IAccountService;

@Service
public class AccountService implements IAccountService {

	private UserRepository _userRepo;

	private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

	@Autowired
	public AccountService(UserRepository userRepo) {
		_userRepo = userRepo;
	}

	@Override
	public Boolean Authenticate(String username, String password) {

		logger.debug("Authenticate({},{})", username, password);

		User u = new User();
		u.setFirstName("Juan");
		u.setLastName("Ayala");

		User saved = _userRepo.save(u);
		return saved.getId() > 0;
	}
}
