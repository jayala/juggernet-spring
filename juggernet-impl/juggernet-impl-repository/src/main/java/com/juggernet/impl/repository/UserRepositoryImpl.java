package com.juggernet.impl.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.juggernet.impl.domain.User;
import com.juggernet.interfaces.domain.IUser;
import com.juggernet.interfaces.repository.IUserRepository;

@Repository
public class UserRepositoryImpl implements IUserRepository {

	private static final Logger logger = LoggerFactory.getLogger(UserRepository.class);

	@PersistenceContext
	private EntityManager em;

	public IUser getUser(Long id) {

		logger.info("getUser({})", id);

		User u = new User() {
			{
				setFirstName("Juan");
				setLastName("Ayala");
			}
		};
		u.setId(id);
		return u;
	}
}