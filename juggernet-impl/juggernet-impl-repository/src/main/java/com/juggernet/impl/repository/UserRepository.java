package com.juggernet.impl.repository;

import org.springframework.data.repository.CrudRepository;

import com.juggernet.impl.domain.User;
import com.juggernet.interfaces.repository.IUserRepository;

public interface UserRepository extends CrudRepository<User, Long>, IUserRepository {

}
